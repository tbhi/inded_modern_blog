# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "inded_modern_blog"
  spec.version       = "0.1.0"
  spec.authors       = [""]
  spec.email         = ["tristan@example.org"]

  spec.summary       = %q{A Simple, Clean and responsive modern blog for jekyll.}
  spec.homepage      = "https://github.com/inded/Jekyll_modern-blog"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", "~> 3.6"

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
end
